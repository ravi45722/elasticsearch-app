var express = require('express');
var router = express.Router();
console.log("after /api")
var elasticSearchHandler = require('../handlers/elasticSearchAPI')
var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline

router.use(elasticSearchOnline)
router.post('/search' , elasticSearchHandler.search)

router.get('/getKPIRules' , elasticSearchHandler.getKPIRules)
router.post('/insertKPIRules', elasticSearchHandler.insertKPIRules)
router.post('/searchKPIRules' , elasticSearchHandler.searchKPIRules)
router.post('/updateKPIRules', elasticSearchHandler.updateKPIRules)
router.post('/deleteKPIRules', elasticSearchHandler.deleteKPIRules)

router.post('/searchUtils', elasticSearchHandler.searchUtils)
router.get('/getUtils', elasticSearchHandler.getUtils)


router.post('/insertDirectoryData' , elasticSearchHandler.insertDirectoryData)
router.get('/getDirectoryData' , elasticSearchHandler.getDirectoryData)
router.post('/searchConfigDiff' ,elasticSearchHandler.searchConfigDiff)

router.post('/searchAuditTrail',elasticSearchHandler.searchAuditTrail)


router.get('/getBreaches', elasticSearchHandler.getBreaches)
router.post('/searchBreaches',elasticSearchHandler.searchBreaches)

router.post('/insertKPIAlarms',elasticSearchHandler.kpiAlarms)

router.post('/countersForKPIBreaches', elasticSearchHandler.getAllCountersForBreaches)

router.post('/dataBasedOnCategory', elasticSearchHandler.getDataBasedonCategory)
router.post('/getDetailedDataForBreaches', elasticSearchHandler.getDetailedDataForBreaches)








//
// router.post('/updateKPI' , elasticSearchHandler.updateKPI)
// router.post('/deleteKPI' , elasticSearchHandler.deleteKPI)
// router.get('/getBreaches' , elasticSearchHandler.getBreaches)
// router.post('/searchBreaches' , elasticSearchHandler.searchBreaches)






module.exports = router;


