/**
 * Created by Radhika on 6/21/2017.
 */
var express = require('express');
var router = express.Router();


//var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline
var influxHandlers = require('../handlers/influxAPI')


router.post("/alerts" , influxHandlers.insertAlarm)
//router.post("/network" , influxHandlers.network)
//router.post("/getAllCountersForKPIBreaches", influxHandlers.getAllCountersForBreaches)


module.exports = router;
