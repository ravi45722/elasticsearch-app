/**
 * Created by radhika on 7/27/2017.
 */
var express = require('express');
var router = express.Router();

var stackStormHandler = require('../handlers/stackStormAPI')
var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline
var readSSLCertificates = require('../libs/middleware').readSSlCertificates
var mongoConnection = require('../libs/middleware').mongoConnection


router.use(readSSLCertificates)
router.get("/rules" , stackStormHandler.getAllRules)
router.get("/ruleDetails", stackStormHandler.getRuleDetails)
router.post("/userRules", elasticSearchOnline,stackStormHandler.userRules) // get rule

router.put('/rules', elasticSearchOnline,stackStormHandler.editRules) //edit rule
router.post('/deleteRules',elasticSearchOnline ,stackStormHandler.deleteRules)
router.get('/actions', elasticSearchOnline,stackStormHandler.actionList)
router.get('/actionDetails',stackStormHandler.actionDetails)



router.post('/rules',elasticSearchOnline,stackStormHandler.addRule) //add rule



router.post('/history',stackStormHandler.history)
router.get('/historyDetails',stackStormHandler.historyDetails)








module.exports = router;
