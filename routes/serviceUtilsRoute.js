/**
 * Created by Radhika on 7/7/2017.
 */
var express = require('express');
var router = express.Router();

var serviceUtilsHandler = require('../handlers/serviceUtilsAPI')
var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline

router.get("/utils" , elasticSearchOnline, serviceUtilsHandler.getServiceUtils)

module.exports = router;
