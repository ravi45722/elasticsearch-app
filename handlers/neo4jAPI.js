const Influx = require('influx');

var neo4j = require('neo4j');
var join = require('join-by-keys')
var db = new neo4j.GraphDatabase('http://neo4j:teledna@172.16.23.27:7474');
var logger = require('../libs/logger').neo4jLogger;


/*

 MATCH (n:Login{UserName:"'+req.body.UserName+'"})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l)-[:Customers]->(c) RETURN
 COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName,CUSTOMER:c.UserName}) as results
 */
/*
 MATCH (n:Login{UserName:"sreepad"})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN
 COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results
 */
function PLV(req, res, next) {

    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }

        var iterate = results[0].results
        var array = join(iterate, ['PRODUCT_NAME', 'VENDOR_NAME'])
        var resultArray = join(array, ['PRODUCT_NAME'])
        var obj = {}
        resultArray.forEach(function (item) {
            var product = item.PRODUCT_NAME
            var vendorArray = item.VENDOR_NAME
            var locationArray = item.LOCATION
            var vendorsObj = {}

            locationArray.forEach(function (eachLocationArr, index) {
                var vendorName = vendorArray[index]
                vendorsObj[vendorName] = locationArray[index]
            })

            obj[product] = vendorsObj

        })
        res.send({"status": 1, "data": obj})

    });
}


function LVP(req, res, next) {
    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        var iterate = results[0].results
        var array = join(iterate, ['LOCATION', 'VENDOR_NAME'])
        var resultArray = join(array, ['LOCATION'])
        var obj = {}
        resultArray.forEach(function (item) {

            var productArrays = item.PRODUCT_NAME
            var vendorArray = item.VENDOR_NAME
            var location = item.LOCATION
            var vendorsObj = {}

            productArrays.forEach(function (eachProductArr, index) {
                var vendorName = vendorArray[index]
                vendorsObj[vendorName] = productArrays[index]
            })
            obj[location] = vendorsObj
        })
        res.send({"status": 1, "data": obj})

    });
}


function getGeoHash(req, res, next) {

    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l)-[:Customers]->(c) return {Product:p.UserName,Location:l.UserName,Customer:c.UserName,geohash:c.geohash}',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }


        res.send({"status": 1, "data": results})

    });
}
/*

 */

//alternate query
/*
 'MATCH(n:`TDNA SMSC`{Name:"TDNA SMSC"})-[r:Floors]->(allFloors)-[r1:Racks]->(allRacks)<-->(node)' +
 ' WITH n,{ allFloors : allFloors, allRacks : allRacks,nodes : COLLECT(node)} AS racks' +
 ' WITH{n : n ,racks : COLLECT(racks)}AS n RETURN n',
 */

function getServerDetails(req, res, next) {
    // {
    //     UserName:”sreepad”,
    //     Location:”Banglore”,
    //     PRODUCT_NAME: “SMSC”,
    //     Customer:”Aircel”
    // }

    db.cypher({
        query: ' MATCH (n{Name:{UserName}})-[r:Floors]->(allFloors)-[r1:Racks]->(allRacks)-[:Contains]->(node) WITH {root:n,Floors : allFloors, Racks : allRacks,nodes : COLLECT(node)} AS racks RETURN racks',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        res.send({"status": 1, "data": results})

    });
}

//  UserName:”sreepad”,
// Location:”Banglore”,
// Product: “SMSC”,
// Customer:”Aircel”

function topologyDisplay(req, res, next) {
    db.cypher({
        query: 'MATCH(n)-[r{Name:"Aircel Chennai"}]-(m)return DISTINCT {S:n.PRODUCT_NAME,R:TYPE(r),E:m.PRODUCT_NAME} as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        } else {
            var node = {};
            var edges = [];
            var id = 0;
            console.log(JSON.stringify(results));
            results.forEach(function (Obj) {
                console.log(JSON.stringify(Obj));
                var link = {};
                link.source = Obj.results.S;
                link.target = Obj.results.E;
                link.type = Obj.results.R;
                link._id = id;
                id++;
                edges.push(link);
                node[Obj.results.S] = {};
                node[Obj.results.E] = {};
                node[Obj.results.S].UserName = Obj.results.S;
                node[Obj.results.E].UserName = Obj.results.E;
            })
            console.log(JSON.stringify(node));
            var nodes = [];
            var IDObj = {};
            var id = 1000;
            Object.keys(node).forEach(function (key) {
                console.log("Key : " + JSON.stringify(node.key));
                var Obj = {};
                Obj.properties = {};
                Obj.labels = [];
                Obj.labels.push("Service Topology");
                Obj.properties['UserName'] = key;
                Obj.properties['Type'] = "Service";
                Obj.id = id;
                Obj._id = id;
                Obj.size = 6104;
                IDObj['key'] = id;
                id++;
                nodes.push(Obj);
            })
            var links = [];
            edges.forEach(function (Obj) {
                Obj['source'] = IDObj[Obj.source];
                Obj['target'] = IDObj[Obj.target];
                links.push(Obj);
            })
            nodes[0].properties.Type = "root";
            var Obj = {
                "nodes": [{
                    "properties": {"UserName": "STP", "Type": "root"},
                    "labels": ["Service Topology"],
                    "id": 1000,
                    "_id": 1000,
                    "size": 6104
                }, {
                    "properties": {"UserName": "SMSC", "Type": "Service"},
                    "labels": ["Service Topology"],
                    "id": 1001,
                    "_id": 1001,
                    "size": 6104
                }], "links": [{"type": "Diameter", "_id": 1, "source": 1000, "target": 1001}]
            };
            res.send(Obj);
        }

    });
}
/*
 $MATCH(n)-[r{Name:"Aircel Chennai"}]-(m)return DISTINCT {N:n.PRODUCT_NAME,R:TYPE(r),M:m.PRODUCT_NAME}



 */

function getIPAndServerDetails(req, res, next) {
    // {
    //     "PRODUCT_NAME":"SMSC",
    //     "LOCATION":"Delhi",
    //     "VENDOR_NAME":"Acision",
    //     "Eth0":"10.178.38.180",
    //     "UserName":"ramya"
    //
    // }
    //MATCH (n:server{PRODUCT_NAME:"SMSC",Eth0:"10.178.38.180"})<-[:Contains]-(R)<-[:Racks]-(F) return {Floor:F.Name,Rack:R.Name,Server:n} as Actual_Data


    db.cypher({
        query: 'MATCH (n:server{PRODUCT_NAME:{PRODUCT_NAME},Eth0:{Eth0}})<-[:Contains]-(R)<-[:Racks]-(F) return {Floor:F.Name,Rack:R.Name,Server:n} as Actual_Data',
        params: {
            PRODUCT_NAME: req.body.PRODUCT_NAME,
            Eth0: req.body.Eth0
        },

    }, function (err, actualData) {
        if (err) {
            return next(err)
        }
        console.log(actualData)
        db.cypher({
            query: ' MATCH (n{Name:{UserName}})-[r:Floors]->(allFloors)-[r1:Racks]->(allRacks)-[:Contains]->(node) WITH {root:n,Floors : allFloors, Racks : allRacks,nodes : COLLECT(node)} AS racks RETURN racks',
            params: {
                UserName: req.body.UserName,
            },

        }, function (err, results) {
            if (err) {
                return next(err)
            }
            console.log(results)
            res.send({"status": 1, "ActualData": actualData, "TotalData": results})

        });
        // res.send({"status": 1, "data": results})

    });


}


function getIPBasedOnProduct(req, res, next) {
    db.cypher({
        query: 'MATCH (n{PRODUCT_NAME:{PRODUCT_NAME}})return n.Eth0 as IP',
        params: {
            PRODUCT_NAME: req.body.PRODUCT_NAME,
        },

    }, function (err, data) {
        if (err) {
            return next(err)
        }

        var arr = []
        data.forEach(function (value, index) {
            arr.push(value.IP)
        })
        var resultArr = arr.filter(function (value) {

            if (value != "" || value != null || value != '') {
                return value
            }


        })
        res.send(resultArr)

    });


}

//LOGICAL ONBOARDING
//receives product_name and returns all groups associated with that product name
function getServerInfoBasedOnProduct(req, res, next) {
    db.cypher({
        query: 'MATCH (n:server{PRODUCT_NAME:{PRODUCT_NAME}}) WHERE exists(n.GROUP) return  ' +
        'n.GROUP as GROUP , COLLECT(n.Eth0) as IP ',
        params: {
            PRODUCT_NAME: req.query.PRODUCT_NAME,
        },

    }, function (err, data) {
        if (err) {
            return next(err)
        }
        db.cypher({
            query: 'MATCH (n:server{PRODUCT_NAME:{PRODUCT_NAME}})-[r]-(m)' +
            'return DISTINCT(type(r)) as connectivity ,m.PRODUCT_NAME as title',
            params: {
                PRODUCT_NAME: req.query.PRODUCT_NAME,
            },
        }, function (err1, data1) {
            if (err1) {
                return next(err1)
            }
            var connArr = []
            var externalElementsArr = []
            data1.forEach(function (item) {
                connArr.push(item.connectivity)
                if (item.title != null) {
                    externalElementsArr.push(item.title)
                }

            })

            res.json({"status": 1, "groups": data, "connectivity": connArr, "externalElements": externalElementsArr})


        })


    });

}




exports.getTopology = topologyDisplay
exports.getGeoHash = getGeoHash
exports.getServerDetails = getServerDetails
exports.getIPAndServerDetails = getIPAndServerDetails
exports.getIPBasedOnProduct = getIPBasedOnProduct
exports.PLV = PLV
exports.LVP = LVP

//logical onboarding
exports.serverInfoBasedOnProduct = getServerInfoBasedOnProduct





