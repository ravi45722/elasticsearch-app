
var moment = require('moment');
var logger = require('../libs/logger').serviceUtilsLogger

function getServiceUtils(req, res) {
    logger.info("All is well with elastic search");
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: 'service-utils',
        type: 'logs',
        body: {
            size: 1000,
            query: {
                match_all:{}
            }
        }
    }, function (err, responseFromElasticSearch) {
        if (err) {
            logger.error("elastic search error" + JSON.stringify(err))
            return next(err)
        } else {
            var array = [];
            responseFromElasticSearch.hits.hits.forEach(function (hit) {
                var jsonObj = hit['_source'];
                jsonObj['_id'] = hit['_id'];
                jsonObj.time = moment.parseZone(hit._source.time).local().format("YYYY-MM-DD HH:mm:ss");
                array.push(jsonObj);
            })
            logger.info("elastic search returns results")
            res.json({"status": 1, "data": array});
        }
    })
}

exports.getServiceUtils = getServiceUtils

