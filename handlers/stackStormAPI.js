/**
 * Created by radhika on 7/27/2017.
 */
var path = require('path')
var request = require('request')


var neo4j = require('neo4j');
var redis = require('redis')
var moment = require('moment')
var ObjectId = require('mongodb').ObjectID


var config = require('config')
var st2Certificate = config.get('stackStorm.certificate')
var st2Key = config.get('stackStorm.key')

var client = redis.createClient(6379, '172.16.23.27');
var db = new neo4j.GraphDatabase('http://neo4j:teledna@172.16.23.27:7474');
var logger = require('../libs/logger').stackStormLogger;


function getCriteriaFromElasticSearch(obj, elasticSearchClient, ruleName, callback) {

    if (obj.trigger.type != 'core.st2.CronTimer' && obj.pack != 'core') {
        console.log("not a cron rule")

        elasticSearchClient.search({
            index: 'lucene_test',
            body: {
                query: {
                    match: {
                        ruleName: ruleName
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying with index test util elasticSearch" + JSON.stringify(err))
                return callback(err)
            }
            if (data) {
                logger.info("elastic search returns results")
                return callback(null, data)


            }
        })
    }
    else {
        logger.info("addRule API : not a cron rule")
        var criteraFound = {
            hits: {
                hits: []
            }
        }
        return callback(null, criteraFound)

    }
}

// displays data seen on the  stackstorm Integrated rules page
function userRules(req, res, next) {
    logger.info("user Rules API called")
    var certificate = req.certificate
    var key = req.key
    var elasticSearchClient = req.elasticSearchClient
    //TO-DO: check if roles are required since api's use the PLV associated with rulename right now
    client.hset(req.body.UserName, 'role', 'groupOwner') //roles are not added to neo4j , aren't being used in the logic flow of the api's
    // })
    client.EXISTS(req.body.UserName + '_rules', function (errFindingKey, keyFound) {
        if (errFindingKey) {
            return next(errFindingKey)
        }
        if (keyFound) {
            logger.info("rules set exists for the user:" + req.body.UserName)
            client.SMEMBERS(req.body.UserName + '_rules', function (errFindingRules, rulesFound) {
                if (errFindingRules) {
                    return next(errFindingRules)
                }
                var ruleArray = []
                rulesFound.forEach(function (ruleId) {
                    // pass ruleId to get details of the rule
                    logger.info("fetching rule details for the rule ID:" + ruleId)
                    ruleDetails(ruleId, certificate, key, function (ruleErr, ruleFound) {
                        if (ruleErr) {
                            return next(ruleErr)
                        }
                        var obj = JSON.parse(ruleFound)
                        // console.log(obj)
                        console.log("the ruleName is" + obj.name)
                        // criteria stored in mongo is trigger.ruleName == rulename . Fetch criteria entered by user from ES
                        getCriteriaFromElasticSearch(obj, elasticSearchClient, obj.name, function (errFindingCriteria, criteriaFound) {

                            if (errFindingCriteria) {
                                return next(errFindingCriteria)
                            }
                            if (criteriaFound.hits.hits.length > 0) {
                                obj.criteria = criteriaFound.hits.hits[0]._source.criteria
                            }

                            else {
                                obj.criteria = {}

                            }
                            /*
                             "parameters": {
                             "file": {"val":"/home/stanley/test19\n"},
                             "hosts": {"val":"172.16.23.27"}
                             }
                             */

                            // loops through the documents to remove PLV from the name before displaying it to the user
                            var splitStringByAmpersand = obj.name.split('&')


                            var parameters = obj.action.parameters

                            var paramObj = {}
                            Object.keys(parameters).forEach(function (key) {
                                paramObj[key] = {"val": parameters[key]}


                            })

                            obj.action.parameters = paramObj
                            if (obj.criteria.IP_ADDRESS != undefined) {

                                var IP = obj.criteria.IP_ADDRESS.pattern.slice(1,-1)
                                obj.criteria.IP_ADDRESS.pattern = IP

                            }
                            if (obj.criteria.CONNECTIVITY != undefined) {

                                var connectivity = obj.criteria.CONNECTIVITY.pattern.slice(1,-1)
                                obj.criteria.CONNECTIVITY.pattern = connectivity

                            }

                            var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                            var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                            var username = PLVUserName[0]
                            var PLVString = PLVUserName[1]
                            var PLV = PLVString.split('-')
                            var PRODUCT_NAME = PLV[0]
                            var VENDOR = PLV[1]
                            var LOCATION = PLV.slice(2, PLV.length)
                            obj.PRODUCT_NAME = PRODUCT_NAME
                            obj.VENDOR = VENDOR
                            obj.LOCATION = LOCATION
                            obj.name = ruleNameTobeDisplayed
                            obj.createdBy = username
                            ruleArray.push(obj)

                            if (rulesFound.length == ruleArray.length) {
                                res.json({"status": 1, "data": ruleArray})
                            }
                        })


                    })

                })

            })

        }
        else {
            logger.info("rules set in redis does not exists for the user:" + req.body.UserName)
            res.send({"status": 0, "msg": "no rules"})
        }
    })

}


function ruleDetails(id, certificate, key, callback) {

    var options = {
        url: 'http://172.16.23.20/api/v1/rules/' + id,
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }

    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return callback(error)
            }
            callback(null, body)

        })
}


function getAllRules(req, res, next) {
    var certificate = req.certificate
    var key = req.key
    var options = {
        url: 'http://172.16.23.20/api/v1/rules',
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }
    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return next(error)
            }
            res.send(body)

        })
}

function insertCriteriaIntoElasticSearch(criteria, ruleName, elasticSearchClient, callback) {


    var splitStringByAmpersand = ruleName.split('&')
    var ruleNameTobeDisplayed = splitStringByAmpersand[1]


    if (Object.keys(criteria).length != 0) {
        var originalCriteria = criteria
        var condition = criteria

        //insert the above along with ruleName to elastic search
        var arr = []
        var parametersArr = []

        // var a = {"trigger.ruleName": {"pattern": ruleName, "type": "equals"}}
        Object.keys(condition).forEach(function (key) {

            var jsonObj = condition[key]

            //var triggerName = key.split('.')
            if (jsonObj.type == 'gt') {
                var symbol = '>'
            }
            else if (jsonObj.type == 'lt') {
                var symbol = '<'
            }
            else if (jsonObj.type == 'neq') {
                var symbol = '!='
            }
            else {
                var symbol = '=='
            }


            // arr.push("myhash[" + "'" + triggerName[1] + "'" + "]" + " " + symbol + " " + jsonObj.pattern)

            if (key == "IP_ADDRESS") {
                jsonObj.pattern = "'" + jsonObj.pattern + "'"
            }

            if (key == "CONNECTIVITY") {
                jsonObj.pattern = "'" + jsonObj.pattern + "'"
            }

            arr.push("myhash[" + "'" + key + "'" + "]" + " " + symbol + " " + jsonObj.pattern)
            parametersArr.push(key)
        })


        var conditionInEs = arr.join(' and ')
        logger.info("the condition inserted to elastic search is")
        var criteria = {}
        criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"} //  & and other special characters  in mongo is best avoided

        elasticSearchClient.index({
            index: 'lucene_test',
            type: 'logs',
            body: {
                "condition": conditionInEs,
                "ruleName": ruleName,
                "parameters": parametersArr,
                "criteria": originalCriteria
            }
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return callback(error)
            } else {

                logger.info("data inserted to elastic search ");
                return callback(null, criteria)
            }
        });

    }
    else {
        criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"}
        logger.info("no criteria specified")
        return callback(null, criteria)
    }
}


function addRuleToMongo(certificate, key, rule, userName, callback) {
    // function called if req.body.rule..trigger is core.st2.CronTimer
    // and if req.body.rule.pack is core
    /*
     "parameters": {
     "file": {"val":"/home/stanley/test19\n"},
     "hosts": {"val":"172.16.23.27"}
     }
     */
    var parameters = rule.action.parameters
    var paramObj = {}
    Object.keys(parameters).forEach(function (key) {

        var getVal = parameters[key]
        // console.log("the key is:" + key)
        // console.log("the value is" + parameters[key])
        // console.log("getVal of val is" + getVal.val)
        // console.log(typeof getVal.val)
        if (getVal.val) {
            //delete parameters[key]
            paramObj[key] = getVal.val


        }
        // console.log(getVal.val)


    })
    logger.info("the paramObj is")
    logger.info(paramObj)


    rule.action.parameters = paramObj

    logger.info("adding rule data to mongoDB via stackstorm API:" + rule)
    var options = {
        url: 'https://172.16.23.20/api/v1/rules',
        method: "POST",
        json: rule,
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,
        }
    }
    request.post(options,
        function (error, response, body) {
            if (error) {
                return callback(error)
            }

            if (body.faultstring != undefined) {
                return callback(null, body)

            }
            else {
                var id = body.id
                logger.info("the ID of the rule inserted to mongo is:" + id)
                client.SADD(userName + '_rules', id, function (err, data) {
                    if (err) {
                        return callback(err)
                    }
                    else {
                        return callback(null, {"status": 1})

                    }

                    // else {
                    //                 var criteria = body.criteria
                    //                  delete criteria['trigger.ruleName']
                    //                  var splitStringByAmpersand = body.name.split('&')
                    //                 var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                    //                 var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                    //                  var username = PLVUserName[0]
                    //                  var PLVString = PLVUserName[1]
                    //                 var PLV = PLVString.split('-')
                    //                  var PRODUCT_NAME = PLV[0]
                    //                  var VENDOR = PLV[1]
                    //                 var LOCATION = PLV.slice(2, PLV.length)
                    //                 body.PRODUCT_NAME = PRODUCT_NAME
                    //                body.VENDOR = VENDOR
                    //                 body.LOCATION = LOCATION
                    //                body.name = ruleNameTobeDisplayed
                    //               return callback(null, {"status": 1, "data": body})
                    //               //  res.json({"status": 1, "data": body})
                    //              }


                })
            }
        })

}


function addRule(req, res, next) {
    logger.info("addRules handler")
    var userName = req.body.UserName
    logger.info("the username is:" + userName)
    var PRODUCT_NAME = req.body.PRODUCT_NAME
    logger.info("the PRODUCT_NAME is:" + PRODUCT_NAME)
    var VENDOR = req.body.VENDOR
    logger.info("the VENDOR is:" + VENDOR)
    var LOCATION = req.body.LOCATION// array
    logger.info("the LOCATION is:" + LOCATION)
    var locations = LOCATION.join('-')
    var ruleName = userName + '_' + PRODUCT_NAME + '-' + VENDOR + '-' + locations + '&' + req.body.rule.name
    req.body.rule.name = ruleName
    if (req.body.rule.trigger.type == 'core.st2.CronTimer' && req.body.rule.pack == 'core') {
        logger.info("cron rule added")
        logger.info(req.body)
        if (req.body.rule.trigger.parameters.month != undefined) {
            delete req.body.rule.trigger.parameters.month
        }
        addRuleToMongo(certificate, key, req.body.rule, req.body.UserName, function (err, data) {
            logger.info("trigger type is cronSt2Timer trigger,  elastic search insertion is not required ")
            if (err) {
                return next(err)
            }
            res.send(data)
        })
    }
    else {
        logger.info("regular rule")
        logger.info(req.body)
        var certificate = req.certificate
        var key = req.key
        var criteria = req.body.rule.criteria
        var elasticSearchClient = req.elasticSearchClient
        insertCriteriaIntoElasticSearch(criteria, ruleName, elasticSearchClient, function (err, data) {
            if (err) {
                return next(err)
            }
            req.body.rule.trigger.type = "elasticsearch.count_event"
            req.body.rule.trigger.ref = "elasticsearch.count_event";
            req.body.rule.criteria = data  //criteria Object returned from the function
            req.body.rule.pack = "elasticsearch"
            addRuleToMongo(certificate, key, req.body.rule, req.body.UserName, function (errWhileCallingSSApi, SSAPISuccess) {
                if (errWhileCallingSSApi) {
                    return next(errWhileCallingSSApi)
                }
                res.send(SSAPISuccess)
            })

        })
    }

}


function editCriteriaInES(elasticSearchClient, ruleName, criteria, callback) {
    if (Object.keys(criteria).length != 0) {
        var originalCriteria = criteria
        console.log("criteris sent ot editCriteris is")
        console.log(originalCriteria)
        var condition = criteria
        var arr = []
        var parametersArr=[]
        Object.keys(condition).forEach(function (key) {
            var jsonObj = condition[key]

            var triggerName = key.split('.')
            if (jsonObj.type == 'gt') {
                var symbol = '>'
            }
            else if (jsonObj.type == 'lt') {
                var symbol = '<'
            }
            else if (jsonObj.type == 'neq') {
                var symbol = '!='
            }
            else {
                var symbol = '=='
            }
            if (key == "IP_ADDRESS") {

                jsonObj.pattern = "'" + jsonObj.pattern + "'"
            }
            if (key == "CONNECTIVITY") {

                jsonObj.pattern = "'" + jsonObj.pattern + "'"
            }

            arr.push("myhash[" + "'" + key + "'" + "]" + " " + symbol + " " + jsonObj.pattern)
            parametersArr.push(key)

            // arr.push("myhash[" + "'" + triggerName[1] + "'" + "]" + " " + symbol + " " + jsonObj.pattern)
        })

        var conditionInEs = arr.join(' and ')
        // console.log("condition inserted to ES is")
        // console.log(conditionInEs)

        var criteria = {}
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        var scriptToAddCriteria = {"inline": "ctx._source.criteria = " + "\"" + originalCriteria + "\"" + ";"}
        var theScript = {
            "inline": "ctx._source.condition = " + "\"" + conditionInEs + "\"" + ";" + "ctx._source.remove(\"criteria\");"

        }
        // the below is a work around to avoid "tried to parse field as object but found a concrete value" which shows up  when using updateByQuery to modify criteria field
        // changed mapping to resolve issue - has not worked
        elasticSearchClient.updateByQuery({
            index: 'lucene_test',
            type: 'logs',
            body: {
                script: theScript,
                query: {
                    term: {
                        ruleName: ruleName,

                    }
                }
            }
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return callback(error)
            } else {
                elasticSearchClient.search({
                    index: 'lucene_test',
                    type: 'logs',
                    body: {
                        query: {
                            match: {
                                ruleName: ruleName
                            }
                        }
                    }
                }, function (errFindingRule, ruleFound) {
                    elasticSearchClient.update({
                        index: 'lucene_test',
                        type: 'logs',
                        id: ruleFound.hits.hits[0]._id,
                        body: {
                            doc: {
                                criteria: originalCriteria,
                                parameters: parametersArr
                            },

                            query: {
                                term: {
                                    ruleName: ruleName
                                }
                            }
                        }
                    }, function (errPartialUpdate, partialUpdateSuccessful) {
                        if (errPartialUpdate) {
                            return callback(errPartialUpdate)
                        }
                        return callback(null, criteria)
                    })
                })


            }
        });

    }
    else {
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        logger.info("no criteria specified")
        return callback(null, criteria)
    }

}


function editRules(req, res, next) {
    logger.info("edit rules API called with the following params")
    logger.info(req.body)
    var certificate = req.certificate
    var key = req.key
    var elasticSearchClient = req.elasticSearchClient
    //when a crontab rule is edited
    if (req.body.rule.trigger.type == 'core.st2.CronTimer' && req.body.rule.pack == 'core') {

        var userName = req.body.UserName
        logger.info("the username is:" + userName)
        var PRODUCT_NAME = req.body.PRODUCT_NAME
        logger.info("the PRODUCT_NAME is:" + PRODUCT_NAME)
        var VENDOR = req.body.VENDOR
        logger.info("the VENDOR is:" + VENDOR)
        var LOCATION = req.body.LOCATION// array
        logger.info("the LOCATION is:" + LOCATION)
        var locations = LOCATION.join('-')
        var ruleName = userName + '_' + PRODUCT_NAME + '-' + VENDOR + '-' + locations + '&' + req.body.rule.name
        req.body.rule.name = ruleName


        var id = req.body.rule.id
        //req.body.rule.id
        console.log(id)
        var options = {
            url: 'https://172.16.23.20/api/v1/rules/' + id,
            agentOptions: {
                cert: certificate,
                key: key,
                rejectUnauthorized: false,

            }
        }

        request.delete(options, function (error, response, body) {
            if (error) {
                logger.error("error encountered while using request module:" + error)
                return next(error)
            }
            client.srem(userName + '_rules', id, function (err, data) {
                if (err) {
                    return next(err)
                }
                console.log(data)
                addRuleToMongo(certificate, key, req.body.rule, req.body.UserName, function (err, data) {
                    logger.info("trigger type is cronSt2Timer trigger,  elastic search insertion is not required ")
                    if (err) {
                        return next(err)
                    }
                    res.send(data)
                })


            })


        })
    }
    //editing a regular rule
    else {
        var id = req.body.rule.id //ref or id
        var originalRuleName = req.body.rule.ref.split('.')[1]
        var userName = req.body.UserName
        var PRODUCT_NAME = req.body.PRODUCT_NAME
        var VENDOR = req.body.VENDOR
        var LOCATION = req.body.LOCATION// array
        var locations = LOCATION.join('-')
        var ruleName = userName + '_' + PRODUCT_NAME + '-' + VENDOR + '-' + locations + '&' + req.body.rule.name
        var criteriaEditedByUser = req.body.rule.criteria
        console.log("the criteria sent is")
        console.log(criteriaEditedByUser)
        var parameters = req.body.rule.action.parameters


        var paramObj = {}
        Object.keys(parameters).forEach(function (key) {
            var getVal = parameters[key]
            paramObj[key] = getVal.val


        })
        logger.info("the paramObj is")
        logger.info(paramObj)
        req.body.rule.action.parameters = paramObj
        req.body.rule.criteria = {"trigger.results.ruleName": {"pattern": ruleName, "type": "equals"}}
        req.body.rule.name = ruleName
        var options = {
            url: 'https://172.16.23.20/api/v1/rules/' + id,
            method: "PUT",
            json: req.body.rule,
            agentOptions: {
                cert: certificate,
                key: key,
                rejectUnauthorized: false

            }
        }
        request.put(options,
            function (error, response, body) {
                if (error) {
                    return next(error)
                }
                logger.info("editing data pertaining to " + ruleName + " in elastic search")
                editCriteriaInES(elasticSearchClient, originalRuleName, criteriaEditedByUser, function (errUpdatingData, updateSuccessful) {
                    if (errUpdatingData) {
                        return next(errUpdatingData)
                    }
                    var id = body.id
                    var setMembers = id;
                    logger.info("deleting the rule id " + id + " from  the user's set in redis")
                    client.SADD(userName + '_rules', setMembers, function (err, data) {
                        if (err) {
                            return next(err)
                        }

                        res.json({"status": 1})
                        // var splitStringByAmpersand = body.name.split('&')
                        //  var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                        //  var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                        //  var username = PLVUserName[0]
                        //  var PLVString = PLVUserName[1]
                        //  var PLV = PLVString.split('-')
                        //  var PRODUCT_NAME = PLV[0]
                        //  var VENDOR = PLV[1]
                        //  var LOCATION = PLV.slice(2, PLV.length)
                        //  body.PRODUCT_NAME = PRODUCT_NAME
                        //  body.VENDOR = VENDOR
                        //  body.LOCATION = LOCATION
                        //  body.name = ruleNameTobeDisplayed
                        // res.json({"status": 1, "data": body})
                    })
                })


            })
    }


}


function deleteRules(req, res, next) {
    //http://172.16.23.20/api/v1/rules/59800c8640c0a003d7456d68
    logger.info("deleteRules API called")
    var certificate = req.certificate
    var key = req.key
    var id = req.body.id
    var elasticSearchClient = req.elasticSearchClient
    var userName = req.body.UserName
    logger.info("post params sent is id and userName:" + req.body.id + ", " + req.body.UserName)
    ruleDetails(id, certificate, key, function (ruleErr, ruleFound) {
        if (ruleErr) {
            return next(ruleErr)
        }
        var obj = JSON.parse(ruleFound)
        var ruleName = obj.name
        logger.info("deleting the rule with the name :" + ruleName)
        var options = {
            url: 'https://172.16.23.20/api/v1/rules/' + id,
            agentOptions: {
                cert: certificate,
                key: key,
                rejectUnauthorized: false,

            }
        }

        request.delete(options,
            function (error, response, body) {
                if (error) {
                    logger.error("error encountered while using request module:" + error)
                    return next(error)
                }
                logger.info("removing rule id from redis set")
                logger.info("removing " + id + " " + "from " + userName + "_rules")
                //delete refID from the hash as well
                client.srem(userName + '_rules', id, function (err, data) {
                    if (err) {
                        return next(err)
                    }
                    //delete name from elastic search
                    elasticSearchClient.deleteByQuery({
                        index: 'lucene_test',
                        type: 'logs',
                        body: {
                            query: {
                                match: {
                                    ruleName: ruleName
                                }
                            }
                        }
                    }, function (errWhileDeleting, dataDeleted) {
                        if (errWhileDeleting) {
                            logger.error(errWhileDeleting)
                            return next(errWhileDeleting)
                        }
                        logger.info("removing" + "" + ruleName + " " + "from elastic search's lucene_index")
                        res.json({"status": 1})

                    })
                    //res.json({"status": 1})

                })


            })
    })
}


function getRuleDetails(req, res, next) {
    logger.info("getRuleDetails API called")
    var certificate = req.certificate
    var key = req.key
    var refID = req.query.refID
    logger.info("query params sent is:" + req.query)
    var options = {
        url: 'http://172.16.23.20/api/v1/rules/' + refID,
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }
    request.get(options,
        function (error, response, body) {
            if (error) {
                logger.error("error encountered while using request module:" + error)
                return next(error)
            }
            res.send(body)

        })
}


function getActionList(req, res, next) {
    var certificate = req.certificate
    var key = req.key
    var elasticSearchClient = req.elasticSearchClient

    var options = {
        url: ' http://172.16.23.20/api/v1/actions',
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }
    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return next(error)
            }
            var body = JSON.parse(body)
            var actionRefList = []
            body.forEach(function (action) {
                actionRefList.push(action.ref)
            })
            var criteriaType = ['equals', 'neq', 'lt', 'gt']
            elasticSearchClient.search({
                index: "stackstorm_criteria",
                body: {
                    size: 100,
                    query: {
                        match_all: {}
                    }
                }
            }, function (err, data) {
                if (err) {
                    return next(err)
                }
                var elasticSearchResults = []
                data.hits.hits.forEach(function (key) {
                    elasticSearchResults.push(key._source.criteria_type)
                })
                res.json({
                    "actionName": actionRefList,
                    "criteriaType": criteriaType,
                    "criteriaKey": elasticSearchResults
                })
            })

        })
}


function history(req, res, next) {
    logger.info("history API called")
    var db = req.db
    var userName = req.body.UserName
    var skip_size = parseInt(req.body.pageSize) * parseInt(req.body.pageNumber - 1)

    /*
     db.action_execution_d_b.aggregate([{ $match:{  "rule.name": {$regex: 'sreepad'} } },{"$project":{
     _status:"$status",_start_timestamp:{$add:[new Date(0),
     { "$divide" : [ "$start_timestamp", 1000 ] }]},"_id":0}},{$sort:{_start_timestamp:-1}},{$skip:1},{$limit:2}])
     */


    db.collection('action_execution_d_b').aggregate([{$match: {"rule.name": {$regex: userName}}},
        {
            "$project": {
                status: "$status", start_timestamp: {$add: [new Date(0), {"$divide": ["$start_timestamp", 1000]}]},
                rule: "$rule", "_id": 1, "web_url": 1
            }
        }, {$sort: {start_timestamp: -1}}, {$skip: skip_size}, {$limit: parseInt(req.body.pageSize)}], function (err, docs) {


        // db.collection('action_execution_d_b').find({"rule.name": {$regex: userName}}, {
        //     "status": 1,
        //     "start_timestamp": 1,
        //     "rule.action": 1,
        //     "rule.name": 1,
        //     "rule.description": 1,
        //     "_id": 1
        // }).sort({"start_timestamp":-1}).skip(skip_size).limit(parseInt(req.body.pageSize)).toArray(function (err, docs) {
        if (err) {
            return next(err)
        }
        if (docs.length == 0) {
            logger.info("there are no documents returned")
            res.json(docs)
        }
        else {
            var ruleArray = []
            var numberOfDocs = docs.length
            logger.info("number of documents returned is"+numberOfDocs)
            docs.forEach(function (doc) {


                if(doc.rule.trigger.type == 'core.st2.CronTimer'){
                   doc.ruleType = 'cron'
                }
                else{
                    doc.ruleType = 'regular'
                }
                var start_timestamp = doc.start_timestamp
                var start_timestamp_date = moment(start_timestamp)
                var dateComponentForStartTimeStamp = start_timestamp_date.utc().format('YYYY-MM-DD');
                var timeComponentForStartTimeStamp = start_timestamp_date.local().format('HH:mm:ss');
                doc.start_timestamp = dateComponentForStartTimeStamp + " " + timeComponentForStartTimeStamp
                //doc.start_timestamp = start_timestamp.toLocaleDateString()+' '+start_timestamp.toTimeString().substring(0, start_timestamp.toTimeString().indexOf("GMT"));
                var splitStringByAmpersand = doc.rule.name.split('&')
                var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                var username = PLVUserName[0]
                var PLVString = PLVUserName[1]
                var PLV = PLVString.split('-')
                var PRODUCT_NAME = PLV[0]
                var VENDOR = PLV[1]
                var LOCATION = PLV.slice(2, PLV.length)
                doc.PRODUCT_NAME = PRODUCT_NAME
                doc.VENDOR = VENDOR
                doc.LOCATION = LOCATION
                doc.rule.name = ruleNameTobeDisplayed
                ruleArray.push(doc)
                if (docs.length == ruleArray.length) {
                    //res.json({"status": 1, "data": ruleArray})
                    res.json({"count":numberOfDocs,"data":ruleArray})
                   // res.json(ruleArray)
                }

            })
        }

    })


}


function historyDetails(req, res, next) {

    var db = req.db
    /*
     db.action_execution_d_b.aggregate([{ $match:{ web_url: {$regex: '5982ac9140c0a0054500f942'} } },
     {"$project":{_status:"$status",_start_timestamp:{$add:[new Date(0),"$start_timestamp"]},
     _web_url:"$web_url",_log:"$log",_result:"$result",_parameters:"$parameters",_rule:"$rule","_id":0,
     execution_time:{$ceil:{$multiply:[{$subtract:["$end_timestamp","$start_timestamp"]},0.000001]}}}}])
     */
    db.collection('action_execution_d_b').aggregate([{$match: {_id: new ObjectId(req.query.id)}}, {
        $project: {
            _status: "$status",
            _start_timestamp: {$add: [new Date(0), {"$divide": ["$start_timestamp", 1000]}]},
            _end_timestamp: {$add: [new Date(0), {"$divide": ["$end_timestamp", 1000]}]},
            _web_url: "$web_url",
            _log: "$log",
            _result: "$result",
            _parameters: "$parameters",
            _rule: "$rule",
            _id: 1,
            _input:"$trigger_instance.payload.results.parameters",
            _input_time:{$ifNull:["$trigger_instance.payload.results.time","$trigger_instance.payload.executed_at"]},
            execution_time: {$ceil: {$multiply: [{$subtract: ["$end_timestamp", "$start_timestamp"]}, 0.000001]}}
        }
    }])

        .toArray(function (err, docs) {
            console.log("the docs rae")
            console.log(docs)
            if (err) {
                return next(err)
            }
            if (docs.length == 0) {
                res.send(docs)
            }
            else {


                var ruleArray = []


                docs.forEach(function (doc) {
                    var start_timestamp = doc._start_timestamp
                    var start_timestamp_date = moment(start_timestamp)
                    var dateComponentForStartTimeStamp = start_timestamp_date.utc().format('YYYY-MM-DD');
                    var timeComponentForStartTimeStamp = start_timestamp_date.local().format('HH:mm:ss');
                    doc._start_timestamp = dateComponentForStartTimeStamp + " " + timeComponentForStartTimeStamp


                    var end_timestamp = doc._end_timestamp
                    var end_timestamp_date = moment(end_timestamp)
                    var dateComponentForEndTimeStamp = end_timestamp_date.utc().format('YYYY-MM-DD')
                    var timeComponentForEndTimeStamp = end_timestamp_date.local().format('HH:mm:ss');
                    doc._end_timestamp = dateComponentForEndTimeStamp + " " + timeComponentForEndTimeStamp
                    var splitStringByAmpersand = doc._rule.name.split('&')
                    var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                    var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                    var username = PLVUserName[0]
                    var PLVString = PLVUserName[1]
                    var PLV = PLVString.split('-')
                    var PRODUCT_NAME = PLV[0]
                    var VENDOR = PLV[1]
                    var LOCATION = PLV.slice(2, PLV.length)
                    doc.PRODUCT_NAME = PRODUCT_NAME
                    doc.VENDOR = VENDOR
                    doc.LOCATION = LOCATION
                    doc.name = ruleNameTobeDisplayed
                    ruleArray.push(doc)
                    if (docs.length == ruleArray.length) {
                        // res.json({"status": 1, "data": ruleArray})
                        res.send(ruleArray)
                    }

                })
            }
        })

}


function actionDetails(req, res, next) {
    //https://172.16.23.20/api/v1/actions/linux.mv
    var certificate = req.certificate
    var key = req.key
    var refID = req.query.refID

    //http://172.16.23.20/api/v1/actions/views/parameters/595cfe75421aa97f086a378e
    var options = {
        //http://172.16.23.20/api/v1/actions/
        //http://172.16.23.20/api/v1/actions/views/overview
        url: 'http://172.16.23.20/api/v1/actions/views/overview/' + refID,
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }

    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return next(error)
            }
            res.json({"status": 1, "data": JSON.parse(body)})

        })
}


exports.getAllRules = getAllRules
exports.getRuleDetails = getRuleDetails
exports.userRules = userRules

exports.addRule = addRule
exports.editRules = editRules
exports.deleteRules = deleteRules
exports.actionList = getActionList
exports.actionDetails = actionDetails


exports.history = history
exports.historyDetails = historyDetails
