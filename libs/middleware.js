

var express = require('express')
var app = express()
var config = require('config')
var elasticsearch = require('elasticsearch');
var elasticSearchClient = new elasticsearch.Client({
    host: config.get('elasticSearch.host'),
    log: config.get('elasticSearch.log')
})

/*
 function validateRequest(req, res, next) {
 var userID = req.userID;
 var projection = {_id: 1, profileStatus:1};


 fetchUserByID(userID, projection, function(err, userData){
 if(err) {
 return next(err);
 }
 else if(!userData) {
 return next(new noSuchUser());
 }
 else {
 var signedinStatus = [11, 21, 31];
 var profileStatus = userData.profileStatus;
 if (signedinStatus.indexOf(profileStatus) == -1) {
 return next(new userNotLoggedIn(profileStatus));
 }
 else {
 next();
 }
 }
 });
 }

 */

function checkIfElasticSearchIsOnline(req,res,next) {
    elasticSearchClient.ping({
        // ping usually has a 3000ms timeout
        requestTimeout: 1000
    }, function (error) {
        if (error) {
            return next(error)

        } else {
            req.elasticSearchClient = elasticSearchClient
            return next()

        }
    });
}


exports.checkIfElasticSearchIsOnline = checkIfElasticSearchIsOnline