/**
 * Created by Radhika on 6/6/2017.
 */
var path = require('path');
var mkdirp = require('mkdirp');
var winston = require('winston');
var expressWinston = require('express-winston');
//var Mail = require('winston-mail').Mail;
var dailyRotateFile =require('winston-daily-rotate-file');
var config = require('./config');

var LogsDir = path.join(__dirname, '..','logs');
var accessFileName = path.join(LogsDir, "access.log");
var errorFileName = path.join(LogsDir, "error.log");
var msgFileName = path.join(LogsDir, "msg.log");

mkdirp.sync(LogsDir);

function myTimestamp() {
    return new Date().toString();
};



/*
 new (winston.transports.DailyRotateFile)({
 filename : fileName,
 datePattern: 'yyyy-MM-dd.',
 prepend: true,
 level : config.logs.loggerLevel,
 json : false,
 maxsize : config.logs.maxSize,
 maxFiles: config.logs.maxFiles,
 timestamp :function(){
 var date=new Date();
 var temp=dt.strftime(date,'%Y-%m-%d %H:%M:%S:%r');
 return temp;
 }
 })
 */
var accessLogObj = {
    transports: [
        new winston.transports.dailyRotateFile({
            filename: accessFileName,
            maxsize: 1048576, //1MB
            maxFiles: 5,
            json: true,
            timestamp: myTimestamp,
            colorize: true
        })
    ],
    meta: true, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}",
    colorStatus: true, // Color the status code, using the Express/morgan color palette (default green, 3XX cyan, 4XX yellow, 5XX red).
    ignoreRoute: function (req, res) {
        if (req.url.startsWith("/docs") || req.url.startsWith("/dummy")){
            // Only for experiment
            return true;
        }
        else {
            return false;
        }
    },
    requestWhitelist: ['url', 'method', 'httpVersion', 'query']
}

var errorLogObj = {
    transports: [
        new winston.transports.File({
            filename: errorFileName,
            maxsize: 1048576, //1MB
            maxFiles: 5,
            json: true,
            timestamp: myTimestamp,
            colorize: true,
            handleExceptions: true,
            exitOnError: false,
        })
    ],
    colorStatus: true,
    requestWhitelist: ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query', 'body']
}



msgLogger = new winston.Logger(msgLogObj);
/*
 msgLogger.add(Mail, {
 to: 'ajith.k@ninestars.in',
 from: 'app9.hindu@ninestars.in',
 subject: 'Test mail',
 host: 'smtp.office365.com',
 port: 587,
 username: 'app9.hindu@ninestars.in',
 password: 'Welcome@987',
 tls: true
 });
 */

config.set('accessLogger', expressWinston.logger(accessLogObj));
config.set('errorLogger', expressWinston.errorLogger(errorLogObj));
//config.set('msgLogger', new (winston.Logger)(msgLogObj));
config.set('msgLogger', msgLogger);
